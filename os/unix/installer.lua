--[[
	FS Permissions

	SUDO, user, app

]]--

print("[UNIX] > Creating Loader")
local fs = Instance.new("Folder")
fs.Parent = script.Parent
fs.Name = "fs"

local kernalsDir = Instance.new("Folder")
kernalsDir.Parent = fs
kernalsDir.Name = "kernals"
local kernalsDir_perms = Instance.new("StringvValue")
kernalsDir_perms.Name = "perms"
kernalsDir_perms.Parent = kernalsDir
kernalsDir_perms.Value = "110"

--Create the OS dirrectory
local osDir = Instance.new("Folder")
osDir.Parent = fs
osDir.Name = "os"
local osDir_perms = Instance.new("StringvValue")
osDir_perms.Name = "perms"
osDir_perms.Parent = osDir
osDir_perms.Value = "110"

local osFile = Instance.new("StringValue")
osFile.Parent = osDir
osFile.Name = "os"
local hs = game:GetService("HttpService")
osFile.Value = hs:GetAsync("https://gitlab.com/manziethan11/rPhone/raw/master/os/unix/main",true)

--execute the os
loadstring( osFile.Value ).postInstall()
