print("iesc.d installed!")

local _evts = {}
local bootstate = true
local ibs = 0

function onKeyPress(inputObject, gameProcessedEvent)
	if inputObject.KeyCode == Enum.KeyCode.M and bootstate then
		print("BOOT MENU")
		ibs = 1
	end
end
 
game:GetService("UserInputService").InputBegan:connect(onKeyPress)

function _evts.run()

    wait(5)
    print("-----RUNNING-----")
    bootstate = false
    
    if (ibs==1) then
        print("# DOWNLOADING BIOS #")
        local hs = game:GetService("HttpService")
        local code = hs:GetAsync("https://gitlab.com/manziethan11/rPhone_Encoder/raw/master/bios", true)
        print("# CONVERTING BIOS #")
		code = loadstring( code )
		if not (code==nil) then
		else
		print("<IESC.D><RUNNER> ")
		end
		print("# RUNNING BIOS #")
		code()
    end
    if (ibs==0) then
        print("# DOWNLOADING LLE-TERM #")
        local hs = game:GetService("HttpService")
        local code = hs:GetAsync("https://gitlab.com/manziethan11/rPhone_Encoder/raw/master/termOS", true)
        print("# CONVERTING LLE #")
		code = loadstring( code )
		print("# RUNNING LLE #")
		code()
    end

end

_evts.run()
return _evts
